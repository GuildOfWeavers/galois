declare module '@gow/galois' {

    // INTERFACES
    // --------------------------------------------------------------------------------------------
    export type Polynom = bigint[]; 

    export interface FiniteField {

        readonly characteristic  : bigint;
        readonly extensionDegree : number;
        readonly elementSize     : number;

        add(x: bigint, y: bigint): bigint;
        sub(x: bigint, y: bigint): bigint;
        mul(x: bigint, y: bigint): bigint;
        div(x: bigint, y: bigint): bigint;
        exp(x: bigint, y: bigint): bigint;
        inv(value: bigint): bigint;

        sum(values: bigint[]): bigint;
        product(values: bigint[]): bigint;
        invBatch(values: bigint[]): bigint[];

        rand(): bigint;

        getRootOfUnity(order: number): bigint;
        getPowerCycle(rootOfUnity: bigint): bigint[];

        addPolys(a: Polynom, b: Polynom): Polynom;
        subPolys(a: Polynom, b: Polynom): Polynom;
        mulPolys(a: Polynom, b: Polynom): Polynom;
        divPolys(a: Polynom, b: Polynom): Polynom;

        mulPolyByConstant(p: Polynom, c: bigint): Polynom;

        evalPolyAt(p: Polynom, x: bigint): bigint;
        evalPolyAtRoots(p: Polynom, rootsOfUnity: bigint[]): bigint[];

        interpolate(xs: bigint[], ys: bigint[]): Polynom;
        interpolateRoots(rootsOfUnity: bigint[], ys: bigint[]): Polynom;
        interpolateQuarticBatch(xSets: bigint[][], ySets: bigint[][]): Polynom[];
    }

    // IMPLEMENTATIONS
    // --------------------------------------------------------------------------------------------
    export interface PrimeField extends FiniteField {
        new(modulus: bigint): PrimeField;
        
        mod(value: bigint): bigint;
    }

    export interface BinaryField extends FiniteField {
        new(modulus: bigint): BinaryField;
    }
}