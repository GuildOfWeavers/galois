"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
// PUBLIC FUNCTIONS
// ================================================================================================
function isPowerOf2(value) {
    return value && (value & (value - 1)) === 0;
}
exports.isPowerOf2 = isPowerOf2;
//# sourceMappingURL=utils.js.map