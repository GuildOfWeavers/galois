// PUBLIC FUNCTIONS
// ================================================================================================
export function isPowerOf2(value: number) {
    return value && (value & (value - 1)) === 0;
}